import onnx
import onnx_tf.backend as tf_backend
import tensorflow as tf

model = onnx.load('leaf_resnet18_fc_weight_out_features_in_features.onnx')
tf_rep = tf_backend.prepare(model)
#saver = tf.train.Saver()
with tf.Session() as sess:
    meta_graph_def = tf.train.export_meta_graph(filename='tfmodel_graph.meta')
    #save_path = saver.save(sess, "tfmodel_params.ckpt")
    #print("Model saved in path: %s" % save_path)

