from __future__ import print_function, division
import torch
from torch.nn import functional as F
#from torchvision import models
from get_result_file_paths import get_result_file_paths
from resnet18_run import share_parser
from collections import OrderedDict
model_dir='./'
save_model_dir = 'model_example'
logits_dir=''
archname='resnet18'
has_weights='False'
is_soft_target='False'
is_distilled='MSETrue'
data_type='test'
epoch = 29
lr = 0.0006
data_separate='50-50'
parser = share_parser()
args = parser.parse_args()
model_path, preds_labels_file = get_result_file_paths(epoch, lr, args.wd, args.dataset, data_separate, archname, data_type, has_weights, logits_dir=logits_dir, model_dir=model_dir, is_soft_target=is_soft_target, is_distill=is_distilled)
save_model_path, _ = get_result_file_paths(epoch, lr, args.wd, args.dataset, data_separate, archname, data_type, has_weights, logits_dir=logits_dir, model_dir=save_model_dir, is_soft_target=is_soft_target, is_distill=is_distilled)
print('loading %s ' % model_path)
state_dict = torch.load(model_path)['state_dict']
print('load %s ' % model_path)

state_dict_rename = OrderedDict()
for k, v in state_dict.items():
    name = k[7:] # remove `module.`
    state_dict_rename[name] = v.cpu()

torch.save(state_dict_rename, save_model_path)
print('save the sate dictionary cpu version at %s' % save_model_path)
