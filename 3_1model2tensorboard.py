import tensorflow as tf
g = tf.Graph()

with g.as_default() as g:
    tf.train.import_meta_graph('tfmodel_graph.meta')

with tf.Session(graph=g) as sess:
    file_writer = tf.summary.FileWriter(logdir='logs/meta_graph', graph=g)
