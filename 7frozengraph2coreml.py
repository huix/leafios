import tfcoreml as tf_converter
tf_converter.convert(tf_model_path = 'frozen_model.pb',
                     mlmodel_path = 'leaf.mlmodel',
                     output_feature_names = ['Softmax:0'],
                     input_name_shape_dict = {'inputs:0' : [1,224, 224, 3]},
                     class_labels=['model_example/labels.txt'])
