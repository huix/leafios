# test in tensorflow19
import tensorflow as tf
from PIL import Image
import numpy as np
from tensorflow.python.lib.io import file_io
from google.protobuf import text_format
from tensorflow.core.framework import graph_pb2
"""
Freeze model reference: https://blog.metaflow.fr/tensorflow-how-to-freeze-a-model-and-serve-it-with-a-python-api-d4f3596b3adc

Add input node: https://stackoverflow.com/questions/41909111/how-to-append-op-at-the-beginning-of-a-tensorflow-graph
"""
"""
img_paht = 'model_example/example.JPG'

img = Image.open(img_paht).resize((224,224)).convert('RGB')
img = np.array(img)
img = img/255.
img = (img-[0.485, 0.456, 0.406])/[0.229, 0.224, 0.225]
img = img.transpose(2,0,1)
img = np.expand_dims(img,axis=0)
"""

def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the 
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it 
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="prefix")
    return graph

output_graph_def = load_graph('frozen_model.pb')


copy = graph_pb2.GraphDef()
copy.CopyFrom(output_graph_def.as_graph_def())

with tf.gfile.GFile("frozen_model19.pb", "wb") as f:
    f.write(copy.SerializeToString())
print("%d ops in the original graph." % len(tf.get_default_graph().as_graph_def().node))
print("%d ops in the final graph." % len(copy.node))
#"""
