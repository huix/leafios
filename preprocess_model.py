import torch.nn as nn
from torch import squeeze, unsqueeze, FloatTensor
from torch.autograd import Variable
from torch.nn import functional as F
import torch
import numpy as np
from torch.autograd import Variable

#mean_tensor = Variable(torch.from_numpy(np.array([[np.ones((224,224))*-0.485, np.ones((224,224))*-0.456, np.ones((224,224))*-0.406],])).type('torch.FloatTensor'))

#std_tensor = Variable(torch.from_numpy(np.array([[np.ones((224,224))*0.229, np.ones((224,224))*0.224, np.ones((224,224))*0.225],])).type('torch.FloatTensor'))

class ResNet18FC_PreProc(nn.Module):
    def __init__(self, resnet):
        super(ResNet18FC_PreProc, self).__init__()
        self.conv1 = resnet.conv1
        self.bn1 = resnet.bn1
        self.relu = resnet.relu
        self.maxpool = resnet.maxpool
        self.layer1 = resnet.layer1
        self.layer2 = resnet.layer2
        self.layer3 = resnet.layer3
        self.layer4 = resnet.layer4
        self.avgpool = resnet.avgpool
        self.fc = resnet.fc
    def forward(self, x):
        #x = x.permute(0,3,1,2)
        #print(x.shape)
        #x = torch.div(x,255.)
        #x = torch.add(x,1,mean_tensor)
        #x = torch.div(x,std_tensor)
        
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        x = F.softmax(x,dim=1)
        return x

