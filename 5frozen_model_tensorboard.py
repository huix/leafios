import tensorflow as tf
from import_pb_to_tensorboard import import_to_tensorboard
import_to_tensorboard('shrink_graph.pb', 'logs/shrink_graph')

"""
def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the 
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it 
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="prefix")
    return graph

graph = load_graph('shrink_graph.pb')
output_graph_def = graph.as_graph_def()
for node in output_graph_def.node:
    #if node.name.find('prefix/Const_10')!=-1:
    if node.name == 'prefix/split_1':
        print(node)
    #for i, inp_node_inp in enumerate(node.input):
    #    if inp_node_inp.find('prefix/Const_10')!=-1:
    #        print(node)
"""

