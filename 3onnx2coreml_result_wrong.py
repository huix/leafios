import onnx
from onnx_coreml import convert
import os

class_labels = []
with open(os.path.join('model_example','labels.txt')) as f:
    for l in f:
        l = l.splitlines()
        class_labels.extend(l)
print(class_labels)
model = onnx.load('leaf_resnet18_29_MSErealdistilled_50-50_checkpoint_lr_0.0006_wd_0.0005_no_weights.onnx')
#tf_rep = prepare(model)
coreml_model = convert(model,
                       mode='classifier',
                       image_input_names=['1'],
                       #preprocessing_args={'is_bgr':False},
                       preprocessing_args={'is_bgr':False, 'red_bias':123.675, 'green_bias':116.28, 'blue_bias':103.53, 'image_scale': 57.375},
                       image_output_names=['215'],
                       deprocessing_args={},
                       class_labels=class_labels,
                       predicted_feature_name='classLabel')
coreml_model.save('leaf.mlmodel')
