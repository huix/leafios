import torch
import os
from torch.autograd import Variable
from collections import OrderedDict

#state_dict_rename = OrderedDict()
model_path='model_example/leaf_resnet18_29_MSErealdistilled_50-50_checkpoint_lr_0.0006_wd_0.0005_no_weights.pth.tar'
save_model_path='model_example/leaf_resnet18_fc_weight_out_features_in_features.pth.tar'
state_dict = torch.load(model_path)
"""
if len(v.size()) == 4:
v = v.permute(2,3,1,0)
state_dict_rename[k] = v
print(state_dict_rename[k].shape)        
elif k == 'fc.weight':
state_dict_rename[k] = v.t()
print(state_dict_rename[k].shape)
else:
state_dict_rename[k] = v
print(state_dict_rename[k].shape)
"""
for k, v in state_dict.items():
    if k == 'fc.weight':
        state_dict[k] = v.t()
        print(state_dict[k].shape)
torch.save(state_dict, save_model_path)
print('save the sate dictionary cpu version at %s' % save_model_path)
