import torch
import os
from torchvision import models
from ResNet18Model_withouttranspose import ResNet18FC
from torch.autograd import Variable
from preprocess_model import ResNet18FC_PreProc
import onnx
#from onnx_coreml import convert

batch_size = 1
model_path='model_example/leaf_resnet18_29_MSErealdistilled_50-50_checkpoint_lr_0.0006_wd_0.0005_no_weights.pth.tar'
arch = 'resnet18'
model = models.__dict__[arch]() #pretrained=True
model.eval()
distilled_model = ResNet18FC(model)
state_dict = torch.load(model_path)
distilled_model.load_state_dict(state_dict)
distilled_model.eval()

model = ResNet18FC_PreProc(distilled_model)
model.eval()

# Input to the model
x = Variable(torch.randn(batch_size, 3, 224, 224), requires_grad=True)

# Export the model
torch_out = torch.onnx.export(model,             # model being run
                               x,                       # model input (or a tuple for multiple inputs)
                               "leaf_resnet18_29_MSErealdistilled_50-50_checkpoint_lr_0.0006_wd_0.0005_no_weights.onnx", # where to save the model (can be a file or file-like object)
                               export_params=True)      # store the trained parameter weights inside the model file
