# test in tensorflow17
import tensorflow as tf
from PIL import Image
import numpy as np
from tensorflow.python.lib.io import file_io
from google.protobuf import text_format
from tensorflow.core.framework import graph_pb2
"""
Freeze model reference: https://blog.metaflow.fr/tensorflow-how-to-freeze-a-model-and-serve-it-with-a-python-api-d4f3596b3adc

Add input node: https://stackoverflow.com/questions/41909111/how-to-append-op-at-the-beginning-of-a-tensorflow-graph
"""
"""
img_paht = 'model_example/example.JPG'

img = Image.open(img_paht).resize((224,224)).convert('RGB')
img = np.array(img)
img = img/255.
img = (img-[0.485, 0.456, 0.406])/[0.229, 0.224, 0.225]
img = img.transpose(2,0,1)
img = np.expand_dims(img,axis=0)
"""

with tf.Session() as sess:
    new_saver = tf.train.import_meta_graph('tfmodel_graph.meta', clear_devices=True)
    #print(sess.graph.get_operations())
    #print(sess.run('Softmax:0', feed_dict={'1:0':img}))

    # We use a built-in TF helper to export variables to constants
    output_graph_def = tf.graph_util.convert_variables_to_constants(
        sess, # The session is used to retrieve the weights
        tf.get_default_graph().as_graph_def(), # The graph_def is used to retrieve the nodes 
        "Softmax".split(",") # The output node names are used to select the usefull nodes
    )
    old_input = None
    for n in output_graph_def.node:
        if n.op == 'Placeholder':
            old_input = n
            break
    print(old_input.name)

    old_input_next = []
    for n in output_graph_def.node:
        if len(n.input) != 0:
            if (len(n.input) >1 and n.input[0] == old_input.name) or (n.input == old_input.name):
                old_input_next.append(n)
                #break
    print(old_input_next)
    """"
    #print(tf.get_default_graph().as_graph_def())
    # Finally we serialize and dump the output graph to the filesystem
    with tf.gfile.GFile("frozen_model.pb", "wb") as f:
        f.write(output_graph_def.SerializeToString())
    print("%d ops in the final graph." % len(output_graph_def.node))
    #"""

    #"""
    #image = tf.image.decode_jpeg(img_paht)
    #img = tf.image.resize_images(image, [224, 224])
    img = tf.placeholder(tf.float32, shape=(1, 224, 224, 3), name='inputs')

    w0 = tf.constant([[[
      [1/255., 0, 0],
      [0, 1/255., 0],
      [0, 0, 1/255.]
    ]]], name="preprocess/weights/0")

    w1 = tf.constant([[[
      [1./0.229, 0, 0],
      [0, 1./0.224, 0],
      [0, 0, 1./0.225]
    ]]], name="preprocess/weights/1")

    b0 = tf.constant([-0.485, -0.456, -0.406], name="preprocess/biases/0")
    b1 = tf.constant([0.0, 0.0, 0.0], name="preprocess/biases/1")

    img = tf.nn.conv2d(img, w0, (1, 1, 1, 1), padding="VALID", name="preprocess/conv2d0/0")
    img = tf.nn.bias_add(img, b0, name="preprocess/bias_add/0")
    img = tf.nn.conv2d(img, w1, (1, 1, 1, 1), padding="VALID", name="preprocess/conv2d1/1")
    img = tf.nn.bias_add(img, b1, name="preprocess/bias_add/1")



    #
    #img = tf.realdiv(img,255.,name='added_div')
    #img = tf.add(img,[-0.485, -0.456, -0.406],name='added_subtract')
    #img = tf.realdiv(img,[0.229, 0.224, 0.225],name='added_div1')
    img = tf.transpose(img, (0,3,1,2), name='preprocess/transpose')


    #img = tf.expand_dims(img, axis=0, name='old_input_begin')
    #print(img)
    #with tf.Session() as sess:
    #    print(sess.graph.get_operations())
    #tf.get_default_graph().get_operations())
    # [<tf.Operation 'inputs' type=Placeholder>, <tf.Operation 'div/y' type=Const>, <tf.Operation 'div' type=RealDiv>, <tf.Operation 'sub/y' type=Const>, <tf.Operation 'sub' type=Sub>, <tf.Operation 'div_1/y' type=Const>, <tf.Operation 'div_1' type=RealDiv>, <tf.Operation 'transpose/perm' type=Const>, <tf.Operation 'transpose' type=Transpose>, <tf.Operation 'old_input_begin/dim' type=Const>, <tf.Operation 'old_input_begin' type=ExpandDims>]

    #tf.get_default_graph().as_graph_def()
    
    #"""
    
    #"""
    old_input_begin_node = tf.get_default_graph().as_graph_def().node[-1]
    
    #old_input_begin_node.input.extend([old_input_next[0].name])
    #print(tf.get_default_graph().as_graph_def().node[-1])
    # https://github.com/tensorflow/tensorflow/blob/28c3c5dd38e3b397c2cf0acdaa6388dcbf0349f7/tensorflow/contrib/session_bundle/exporter.py
    copy = graph_pb2.GraphDef()
    copy.CopyFrom(tf.get_default_graph().as_graph_def())
    new_input_next_idx = None
    count = 0
    for n, node in enumerate(copy.node):
        if node.name == old_input_next[0].name:
            for i, inp_node_inp in enumerate(node.input):
                if inp_node_inp == old_input.name:
                    copy.node[n].input[i] = old_input_begin_node.name
                    new_input_next_idx = n
                    count += 1
                    break
        elif node.name == old_input.name:
            del copy.node[n]
            count += 1
        if count == 2:        
            break
    print(copy.node[new_input_next_idx])
    #graph_pbtxt = text_format.MessageToString(copy)
    #file_io.write_string_to_file('frozen_model.pbtxt', graph_pbtxt)

    with tf.gfile.GFile("frozen_model.pb", "wb") as f:
        f.write(copy.SerializeToString())
    print("%d ops in the original graph." % len(tf.get_default_graph().as_graph_def().node))
    print("%d ops in the final graph." % len(copy.node))
    #"""
